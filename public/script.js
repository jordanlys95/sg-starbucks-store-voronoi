
const mapboxToken = "pk.eyJ1Ijoiam9yZGFubHlzOTUiLCJhIjoiY2thMjRoZjk5MDhkejNmbzV2NnQ3OGR2aCJ9.lKziTfam_bfJEQ43Eh3rig";
mapboxgl.accessToken = mapboxToken;
var options = {
  bbox: [103.580737, 1.13291, 104.093415, 1.473158]
};
var map = new mapboxgl.Map({
  container: "map",
  style: "mapbox://styles/mapbox/light-v10",
  center: [103.8163, 1.3426],
  zoom: 10,
  hash: true
});

class ContourLegend {
  onAdd(){
    this._map = map;
    this._container = document.createElement('div');
    this._container.className = 'legend';
    this._container.innerHTML = '<div style="background-color: #00704a">15mins</div><div style="background-color: #328c6e">30mins</div><div style="background-color: #66a992">60mins</div>';
    return this._container;
  }
}
map.addControl(new ContourLegend(), 'top-right')

map.on("style.load", function () {
  map.addSource("stores", { type: "geojson", data: 'sb_store_locations.geojson' });
  map.addLayer({
    id: "stores",
    type: "circle",
    source: "stores",
    paint: {
      "circle-color": "#00704A",
      "circle-radius": 6,
      "circle-stroke-color": "#27251F",
      "circle-stroke-opacity": 1,
      "circle-stroke-width": 2
    }
  });
  map.addSource('isoWalking', {
    type: 'geojson',
    data: 'sb_store_contour.geojson'
  });
  map.addLayer({
    'id': 'walking',
    'type': 'line',
    'source': 'isoWalking',
    'layout': {},
    'paint': {
      "line-width": 2,
      "line-color": "#27251F"
    }
  });
  map.addSource('selectedStore', {
    type: 'geojson',
    data: {
      'type': 'FeatureCollection',
      'features': []
    }
  });
  map.addLayer({
    'id': 'selected',
    'type': 'fill',
    'source': 'selectedStore',
    'layout': {},
    'paint': {
      'fill-opacity': 0.5,
      'fill-color': ['get', 'color']
    }
  }, 'water');
  map.setLayoutProperty('poi-label', 'visibility', 'none'); //remove random shop names
  var walking_isochrone = [];

  var walking_promises = [];

  var main_polygon;
  
  

})


map.on("load", function () {
  map.on('mouseenter', 'stores', function () {
    map.getCanvas().style.cursor = 'pointer';
  });

  // Change it back to a pointer when it leaves.
  map.on('mouseleave', 'stores', function () {
    map.getCanvas().style.cursor = '';
  });

  map.on("click", "stores", function (e) {
    new mapboxgl.Popup()
      .setLngLat(e.lngLat)
      .setHTML(e.features[0].properties.name)
      .addTo(map);

    fetch('https://api.mapbox.com/isochrone/v1/mapbox/walking/' + e.lngLat.lng +','+ e.lngLat.lat + '.json?contours_minutes=15,30,60&contours_colors=00704a,328c6e,66a992&polygons=true&access_token=' + mapboxToken)
      .then(response => response.json())
      .then(featureCollection => {
        map.getSource('selectedStore').setData(featureCollection)
      })
  });
});

function compileContour(){
  fetch('sb_store_locations.geojson')
    .then(response => response.json())
    .then(sb_store_collection => {
      turf.featureEach(sb_store_collection, function (currentFeature, featureIndex) {
        var point = turf.getCoord(currentFeature).join(',');
        // Walking
        var promise_walking = fetch('https://api.mapbox.com/isochrone/v1/mapbox/walking/' + point + '.json?contours_minutes=15&polygons=true&access_token=' + mapboxToken)
          .then(response => response.json())
          .then(({ features }) => {
            walking_isochrone.push(features[0])
          })
        walking_promises.push(promise_walking);

        Promise.all(walking_promises)
          .then(() => {
            var walking_union = turf.union(...walking_isochrone);
            map.getSource('isoWalking').setData(turf.flatten(turf.featureCollection([walking_union])));
          })

      })
    });
}




