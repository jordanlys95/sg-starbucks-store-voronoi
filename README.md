## A map of Singapore split by the nearest Starbucks.
Made using turf.js.

See Part 1. [Nearest McDonald's](https://gitlab.com/jordanlys95/sg-mcdonalds-store-voronoi)

Inspired by [Joseph Tang's Singapre MRT Map](https://github.com/jtlx/singapore-mrt-voronoi)

### Attribution
- [Starbucks Singapore Store Outlets](https://www.starbucks.com.sg/coffeehouse/store-locator) accessed on 11 May 2020.
